import numpy as np 
import pandas
import subprocess
import time 
import os 


gas_i = 1 #CO2
Conc = [0.,0.,0.,0.,0.]  #H20 CO2 CO CH4 Soot
Conc[gas_i] = .01
gas_X = ['H20', 'CO2', 'CO', 'CH4']
Temp = 300.

nameInputSNB  = 'SNBINPUT'
nameOutputSNB = 'SNBTRANS'


for length in [1.,2.,10.,90.]:

    #set ipput
    fin = open(nameInputSNB,'w')
    fin.write('1\n')
    fin.write(' {:}\t{:}\t1.\t{:}\t{:}\t{:}\t{:}\t{:}\n'.format(length, float(Temp),Conc[0],Conc[1],Conc[2],Conc[3],Conc[4]))
    fin.close()

    #run SNB
    command = './SNB_here'       
    pop = subprocess.Popen(command)
    while pop.poll() != 0:
        time.sleep(1)

    os.rename('./SNBTRANS', './SNBTRANS_{:02d}cm'.format(int(length)))


mm01 = pandas.read_csv('./SNBTRANS_01cm', header=None, delim_whitespace=True, names=['nu','trans'])
mm02 = pandas.read_csv('./SNBTRANS_02cm', header=None, delim_whitespace=True, names=['nu','trans'])
mm10 = pandas.read_csv('./SNBTRANS_10cm', header=None, delim_whitespace=True, names=['nu','trans'])
mm90 = pandas.read_csv('./SNBTRANS_90cm', header=None, delim_whitespace=True, names=['nu','trans'])


kappa01 = -1*np.log(mm01['trans'])/1.e-2
kappa02 = -1*np.log(mm02['trans'])/2.e-2
kappa10 = -1*np.log(mm10['trans'])/1.e-1
kappa90 = -1*np.log(mm90['trans'])/9.e-1

tau01 = -1*np.log(mm01['trans'])
tau02 = -1*np.log(mm02['trans'])
tau10 = -1*np.log(mm10['trans'])
tau90 = -1*np.log(mm90['trans'])



#inu = np.abs(mm01['nu']-666).argmin()
inu = np.abs(mm01['nu']-2350).argmin()
#inu = np.abs(mm01['nu']-5000).argmin()

print('nu = {:.1f} - lambda = {:.3f}'.format(mm01['nu'].loc[inu], 1.e4/mm01['nu'].loc[inu]))
print('lenght(cm)  trans  kappa(m-1) ')
print('{:.2f} {:.2e} {:.3f} '.format(1.e-2, mm01['trans'].loc[inu], kappa01.loc[inu],)) 
print('{:.2f} {:.2e} {:.3f} '.format(2.e-2, mm02['trans'].loc[inu], kappa02.loc[inu],))
print('{:.2f} {:.2e} {:.3f} '.format(1.e-1, mm10['trans'].loc[inu], kappa10.loc[inu],)) 
print('{:.2f} {:.2e} {:.3f} '.format(9.e-1, mm90['trans'].loc[inu],kappa90.loc[inu], ))

