program TRANSCK
!!
!! This program calculates the mean transmissivities, averaged on  spectral narrow-bands, of an
!! atmospheric H2O/N2 uniform column, using the CK model and parameters from the file CKH2O.
!! The H2O molar fraction XH2O, the temperature T, and the length L1 of the column are
!! interactively read, and also the name of the result file. This file provides for each narrow-band, its
!! width (cm-1),  its wave-number centre (cm-1), and the mean transmissivity.
!!
IMPLICIT NONE
!!
integer(4)::ngauss,ntemp,nnu,nvf
!! DECLARATION OF THE VARIABLES
integer(4):: IGAUSS, INU, IVF, ITEMP, I1, I2
real(8):: XH2O, L1, T, CT1, CT2, CX1, CX2
real(8),allocatable:: K1(:,:,:,:),DNU1(:),SIG1(:),STAU(:),TEMP(:),XGAZ(:),AMEGA(:),kstar(:,:)
character(len=72):: NAME1, NAME2
!! ENTERING THE DATAS
WRITE (6,*) 'ENTER XH2O, T (K), L (CM) :'
READ (5,*) XH2O, T, L1
NAME1 = 'CKH2O'
WRITE (6,*) 'ENTER THE NAME OF THE RESULT FILE :'
READ (5,'(A72)') NAME2
call readparameters
call interpol_linear(T,TEMP,NTEMP,CT1,CT2,I1,'TEMPERATURE')
call interpol_linear(XH2O,XGAZ,NVF,CX1,CX2,I2,'VOLUMETRIC FRACTION')
!! CALCULATING THE TRANSMITTIVITY
kstar=CT1*CX1*K1(I1,I2,:,:)+CT2*CX1*K1(I1+1,I2,:,:)+CT1*CX2*K1(I1,I2+1,:,:)+CT2*CX2*K1(I1+1,I2+1,:,:)
kstar=kstar*XH2O*L1/(T*QH2O(T))
stau=matmul(exp(-kstar),amega)
OPEN(UNIT=15,FILE=NAME2)
DO INU=1, NNU
   WRITE(15,'(0PF5.1,1X,0PF8.1,1X,1PE10.4)') DNU1(INU), SIG1(INU), STAU(INU)
ENDDO
CLOSE(15)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine readparameters
implicit none
integer(4)::igauss,itemp,ivf
open(1,file=name1,status='OLD')
read(1,*)
read(1,*) nnu,ngauss,ntemp,nvf
allocate(dnu1(nnu),sig1(nnu),STAU(nnu),temp(ntemp),xgaz(nvf),amega(ngauss),k1(1:NTEMP,1:NVF,1:NNU,1:NGAUSS))
allocate(kstar(1:nnu,1:ngauss))
read(1,*)
read(1,*)(amega(igauss),igauss=1,ngauss)
read(1,*)
read(1,*)(temp(itemp),itemp=1,ntemp)
read(1,*)
read(1,*)(xgaz(ivf),ivf=1,nvf)
do inu=1,nnu
   read (1,*) SIG1(INU), DNU1(INU)
   do igauss=1,ngauss
      read(1,*)
      do ivf=1,nvf
           read(1,'(9(5(1PE12.6,1X),/),3(1PE12.6,1X))') (K1(ITEMP,IVF,INU,IGAUSS),ITEMP=1,NTEMP)
      enddo
   enddo
enddo
close(1)
end subroutine readparameters
!!!!!!!!!
subroutine interpol_linear(t,ttab,nttab,c1,c2,itab,nomvariable)
implicit none
character(len=*)::nomvariable
real(8)::t,ttab(:),c1,c2
integer(4)::nttab,itab
if(t<ttab(1).or.t>ttab(nttab))then
   write(6,*) 'ERROR : THE '//trim(nomvariable)//' IS EITHER TOO LOW'
   write(6,*) 'OR TOO HIGH'
   stop
endif
itab=1
do while(t>ttab(itab+1))
     itab=itab+1
enddo
c1=(ttab(itab+1)-t)/(ttab(itab+1)-ttab(itab))
c2=(t-ttab(itab))/(ttab(itab+1)-ttab(itab))
end subroutine interpol_linear
!!!!!!!
function QH2O(t) result(q)
implicit none
integer(4)::i
real(8)::t,q
real(8),dimension(0:6)::lnt,a=(/-14.0874691574179_8,37.9243248539882_8,-42.6817978731789_8,&
       25.3302448517916_8,-8.10851262935532_8,1.33106871720535_8,-0.0872981051095757_8/)
lnt(0)=1._8;lnt(1)=log10(t)
do i=2,6;lnt(i)=lnt(i-1)*lnt(1);enddo
q=10._8**(dot_product(a,lnt))
end function QH2O
!!!!!!!
end program  TRANSCK
